/**
 * Jeu du puissance 4
 * @author marloxDev
 */

import java.util.Scanner;

 public class Puissance4 {

    private static String[] jeu = new String[42]; 
    private static String j1, j2, jAct;    
    private static int nbj;

    public static void main(String[] args) {
        intro();
        jouer();
    }

    /**
     * Lancement de la partie de p4
     */
    public static void jouer(){

        remplirJeu();
        TestJeuGagnantH();
        TestJeuGagnantV();
        TestJeuGagnantD();
        System.out.println("--- Tests effectués ---");
        attendre(800);
        affichage();

        Scanner sc = new Scanner(System.in);
        System.out.print("Entrez le nom du premier joueur : ");
        j1 = sc.next();
        System.out.print("Entrez le nom du second joueur : ");
        j2 = sc.next();

        jAct = "Joueur";
        nbj = (int) (Math.random() *2);
        jAct = j2;
        if (nbj == 1){
            jAct = j1;
        }

        System.out.println("Début de la partie de puissance 4 ! ");
        int caseJ = -1;
        int joue = 0;
        while (peutJouer() == true){
            caseJ = -1;
            
            affichage();
            jAct = changeJ(jAct);
            aQuiDeJouer(jAct);

            while ((caseJ > 7 || caseJ < 1) || (lJouable(caseJ) == -1)){
                System.out.print("Dans quelle colonne souhaitez-vous jouer ? ");
                caseJ = sc.nextInt();
            }
            veutJouer(jAct, lJouable(caseJ));
        }

        affichage();
        System.out.println(jAct+" \033[1;32ma gagné la partie !!\033[0m");
    }

    /**
     * Méthode qui permet de placer la piece dans le plateau
     * @param jAct la colonne dans laquelle le joueur veut joeur
     * @param caseJ le nom du joueur actuel
     */
    public static void veutJouer(String jAct, int caseJ){
        if (jAct.equals(j1)){
            jeu[caseJ] = "\033[1;31mo";
        } else {
            jeu[caseJ] = "\033[1;33mo";
        }
    }

    /**
     * Méthode qui indique si une colonne est jouable et si
     * @param caseJ
     * @return
     */
    public static int lJouable(int caseJ){
        int ret = -1;
        caseJ = caseJ - 1;
        if (caseJ != -1){
            if (jeu[caseJ].equals("\033[1;35m.")){
                ret = caseJ;
                if (jeu[(caseJ+7)].equals("\033[1;35m.")){
                    ret = (caseJ+7);
                    if (jeu[(caseJ+14)].equals("\033[1;35m.")){
                        ret = (caseJ+14);
                        if (jeu[(caseJ+21)].equals("\033[1;35m.")){
                            ret = (caseJ+21);
                            if (jeu[(caseJ+28)].equals("\033[1;35m.")){
                                ret = (caseJ+28);
                                if (jeu[(caseJ+35)].equals("\033[1;35m.")){
                                    ret = (caseJ+35);
                                }
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }

    /**
     * 
     * @return
     */
    public static boolean peutJouer(){
        boolean ret = true;
        int i = 0;
        int sum = 0;
        while (i < jeu.length){
            if (jeu[i].equals("\033[1;35m.") == true){
                sum++;
            }
            i++;
        }
        if (sum < 1){
            ret = false;
        }

        if (jeuGagnant() == true){
            ret = false;
        }

        return ret;
    }

    public static boolean jeuGagnant(){
        boolean ret = false;
        int i = 0;
        int j = 0;
        int k = 0;
        //Horizontales
        while (j < 6){
            k = 0;
            while (k < 4){
                if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
                    if ((jeu[i].equals(jeu[i+1])) && (jeu[i+1].equals(jeu[i+2])) && (jeu[i+2].equals(jeu[i+3]))){
                        jeu[i] = "\033[1;32mo";
                        jeu[i+1] = "\033[1;32mo";
                        jeu[i+2] = "\033[1;32mo";
                        jeu[i+3] = "\033[1;32mo";
                        ret = true;
                    }
                }
                i++;
                k++;
            }
            i = i + 3;
            j++;
        }

        //Verticales
        i = 0;
        j = 0;
        k = 0;
        while (j < 7){
            k = 0;
            while (k < 3){
                if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
                    if ((jeu[i].equals(jeu[i+7])) && (jeu[i+7].equals(jeu[i+14])) && (jeu[i+14].equals(jeu[i+21]))){
                        jeu[i] = "\033[1;32mo";
                        jeu[i+7] = "\033[1;32mo";
                        jeu[i+14] = "\033[1;32mo";
                        jeu[i+21] = "\033[1;32mo";
                        ret = true;
                    }
                }
                i = i + 7;
                k++;
            }
            j++;
            i = j;
        }

        //Diagonales
        i = 0;
        j = 0;
        boolean res = false;

        while (i < 40 && ret == false){
            
            if ((i > 2 && i < 7) || (i > 9 && i < 14) || (i > 16 && i < 21)){
                if (diagGagnanteBG(i) == true){
                    ret = true;
                }
            } else {
                if ((i >= 0 && i < 4) || (i >= 7 && i < 11) || (i >= 14 && i < 18)){
                    if (diagGagnanteBD(i) == true){
                        ret = true;
                    }
                }
            }

            if ((i > 20 && i < 25) || (i > 27 && i < 32) || (i > 34 && i < 39)){
                if (diagGagnanteHD(i) == true){
                    ret = true;
                }
            } else {
                if ((i > 23 && i < 28) || (i > 30 && i < 35) || (i > 37 && i <= 41)){
                    if (diagGagnanteHG(i) == true){
                        ret = true;
                    }
                }
            }
            i++;
        }
        return ret;
    }

/*-----------------------------
| .0 | .1 | .2 | .3 | .4 | .5 | 6. |
-----------------------------
| .7 | .8 | .9 | .10 | .11 | .12 | .13 |
-----------------------------
| .14 | .15 | .16 | .17 | .18 | .19 | .20 |
-----------------------------
| .21 | .22 | .23 | .24 | .25 | .26 | .27 |
-----------------------------
| .28 | .29 | .30 | .31 | .32 | .33 | .34 |
-----------------------------
| .35 | .36 | .37 | .38 | .39 | .40 | .41 |
----------------------------- */

    /**
     * Test si une diagonale en partant de l'indice i est gagnante
     * @param i l'indice duquel tester la diagonale 
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteHG(int i){
        boolean ret = false;
        if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
            if ((jeu[i].equals(jeu[i-8])) && (jeu[i-8].equals(jeu[i-16])) && (jeu[i-16].equals(jeu[i-24]))){
                jeu[i] = "\033[1;32mo";
                jeu[i-8] = "\033[1;32mo";
                jeu[i-16] = "\033[1;32mo";
                jeu[i-24] = "\033[1;32mo";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Test si une diagonale vers le haut droit en partant de l'indice i est gagnante
     * @param i l'indice duquel tester la diagonale 
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteHD(int i){
        boolean ret = false;
        if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
            if ((jeu[i].equals(jeu[i-6])) && (jeu[i-6].equals(jeu[i-12])) && (jeu[i-12].equals(jeu[i-18]))){
                jeu[i] = "\033[1;32mo";
                jeu[i-6] = "\033[1;32mo";
                jeu[i-12] = "\033[1;32mo";
                jeu[i-18] = "\033[1;32mo";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Test si une diagonale en partant de l'indice i est gagnante
     * @param i l'indice duquel tester la diagonale 
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteBD(int i){
        boolean ret = false;
        if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
            if ((jeu[i].equals(jeu[i+8])) && (jeu[i+8].equals(jeu[i+16])) && (jeu[i+16].equals(jeu[i+24]))){
                jeu[i] = "\033[1;32mo";
                jeu[i+8] = "\033[1;32mo";
                jeu[i+16] = "\033[1;32mo";
                jeu[i+24] = "\033[1;32mo";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Test si une diagone en partant de l'indice i est gagnante
     * @param i l'indice duquel tester la diagonale 
     * @return true si la diagonale est gagnante
     */
    public static boolean diagGagnanteBG(int i){
        boolean ret = false;
        if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
            if ((jeu[i].equals(jeu[i+6])) && (jeu[i+6].equals(jeu[i+12])) && (jeu[i+12].equals(jeu[i+18]))){
                jeu[i] = "\033[1;32mo";
                jeu[i+6] = "\033[1;32mo";
                jeu[i+12] = "\033[1;32mo";
                jeu[i+18] = "\033[1;32mo";
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Test de la méthode qui vérifie si un jeu est gagnant dans toutes les positions horizontales possibles
     * @return ret true si une ligne horizontale est gagnante
     */
    public static boolean TestJeuGagnantH(){
        boolean ret = false;
        int i = 0;
        int j = 0;
        int k = 0;
        while (j < 6){
            k = 0;
            while (k < 4){
                if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
                    if ((jeu[i].equals(jeu[i+1])) && (jeu[i+1].equals(jeu[i+2])) && (jeu[i+2].equals(jeu[i+3]))){
                        jeu[i] = "\033[1;32mo";
                        jeu[i+1] = "\033[1;32mo";
                        jeu[i+2] = "\033[1;32mo";
                        jeu[i+3] = "\033[1;32mo";
                        ret = true;
                    }
                }
                jeu[i] = "\033[1;33mh";
                int l = 0;
                while(l < 8){
                    System.out.println();
                    System.out.println();
                    l++;
                }
                affichage();
                attendre(50);
                jeu[i] = "\033[1;35m.";
                System.out.println(i);
                i++;
                k++;
            }
            i = i + 3;
            j++;
        }

        return ret;
    }

    /**
     * Test de la méthode qui vérifie si un jeu est gagnant dans toutes les positions verticales possibles
     * @return ret true si une ligne verticale est gagnante
     */
    public static boolean TestJeuGagnantV(){
        boolean ret = false;
        int i = 0;
        int j = 0;
        int k = 0;
        while (j < 7){
            k = 0;
            while (k < 3){
                if ((jeu[i].equals("\033[1;31mo")) || (jeu[i].equals("\033[1;33mo"))){
                    if ((jeu[i].equals(jeu[i+7])) && (jeu[i+7].equals(jeu[i+14])) && (jeu[i+14].equals(jeu[i+21]))){
                        jeu[i] = "\033[1;32mo";
                        jeu[i+8] = "\033[1;32mo";
                        jeu[i+16] = "\033[1;32mo";
                        jeu[i+24] = "\033[1;32mo";
                        ret = true;
                    }
                }
                jeu[i] = "\033[1;33mv";
                int l = 0;
                while(l < 8){
                    System.out.println();
                    System.out.println();
                    l++;
                }
                affichage();
                attendre(50);
                jeu[i] = "\033[1;35m.";
                System.out.println(i);
                i = i + 7;
                k++;
            }
            j++;
            i = j;
        }

        return ret;
    }

    /**
     * Test de la méthode qui vérifie si un jeu est gagnant dans toutes les positions diagonales possibles
     * @return ret true si une ligne diagonale est gagnante
     */
    public static boolean TestJeuGagnantD(){
        boolean ret = false;
        int i = 0;
        while (i < 42){
            jeu[i] = "\033[1;33md";
            affichage();
            attendre(50);
            jeu[i] = "\033[1;35m.";

            i++;
        }


        return ret;
    }

    /**
     * Permet de patienter pendant x temps en ms
     * @param temps le temps à patienter en ms
     */
   public static void attendre (long temps){
        long t1 = System.currentTimeMillis();
        long t2 = 0;
        long diffT = 0;
        diffT = t2 - t1;
        while (diffT < temps){
            t2 = System.currentTimeMillis();
            diffT = t2 - t1;
        }
     }

     /*
    * Affiche les println de début de jeu + les crédits...
    */
    public static void intro (){
        int version = 1;
        String vBot = "En developpement";
        String etat = "Version JcJ opérationnelle";

        System.out.println();
        System.out.println("\033[0m\033[0;32mLancement de Puissance4.java");
        attendre(500);
        System.out.println("Version actuelle : \033[0;35mV" + version + " \033[0;32mVersion Bot : \033[0;35m" + vBot + "\033[0;32m");
        attendre(500);
        System.out.println("Developpé par MEMAIN Maël");
        attendre(500);
        System.out.print("Début : 31/03/2023 ");
        attendre(200);
        System.out.println("Fin : 04/04/2023");
        attendre(500);
        System.out.println("Etat : " + etat);
        System.out.println();
        attendre(1500);
     }

    /**
     * Méthode qui permet d'afficher le nom du joueur qui doit joeur
     * @param jAct le nom du joueur actuel
     */
    public static void aQuiDeJouer(String jAct){
        if (jAct.equals(j1)){
            System.out.println("C'est à \033[1;31m"+jAct+"\033[1;37m de jouer !");

        } else {
            System.out.println("C'est à \033[1;33m"+jAct+"\033[1;37m de jouer !");
        }
    }

    /**
     * Méthode qui permet de changer de joueur à chaque tour
     */
    public static String changeJ(String jAct){
        String ret;
        if (jAct == j2){
            ret = j1;
        } else {
            ret = j2;
        }
        return ret;
    }


    /**
     * Méthode permettant de remplir le jeu avec les numéros de a à p pour les 16 cases
     */
    public static void remplirJeu(){
        int i = 0;
        
        while (i < jeu.length){
            jeu[i] = "\033[1;35m.";
            i++;
        }
    }

    /**
     * Methode permettant d'afficher le jeu
     */
    public static void affichage(){
        int i = 0;
        int j = 0;

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("\033[1;32m  1   2   3   4   5   6   7  ");
        System.out.println("\033[1;37m-----------------------------");

        while (j < 6){
            System.out.println("| "+jeu[i]+"\033[1;37m | "+jeu[i+1]+"\033[1;37m | "+jeu[i+2]+"\033[1;37m | "+jeu[i+3]+"\033[1;37m | "+jeu[i+4]+"\033[1;37m | "+jeu[i+5]+"\033[1;37m | "+jeu[i+6]+"\033[1;37m |");
            System.out.println("\033[1;37m-----------------------------");
            j++;
            i = i + 7;
        }
        System.out.println();
    }
    
 }

 /*-----------------------------
| .0 | .1 | .2 | .3 | .4 | .5 | 6. |
-----------------------------
| .7 | .8 | .9 | .10 | .11 | .12 | .13 |
-----------------------------
| .14 | .15 | .16 | .17 | .18 | .19 | .20 |
-----------------------------
| .21 | .22 | .23 | .24 | .25 | .26 | .27 |
-----------------------------
| .28 | .29 | .30 | .31 | .32 | .33 | .34 |
-----------------------------
| .35 | .36 | .37 | .38 | .39 | .40 | .41 |
----------------------------- */